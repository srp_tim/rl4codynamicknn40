#!/usr/bin/env bash
#SBATCH --job-name=20-50knndynamic40
#SBATCH --output=20-50knndynamic40%j.log
#SBATCH --error=20-60knndynamic40%j.err
#SBATCH --mail-user=alhermi@uni-hildesheim.de
#SBATCH --partition=STUD
#SBATCH --gres=gpu:1

#!/usr/bin/env bash


DEVICES="0"

#WANDB_MODE=dryrun
wandb login 9815f3b0e8ce08b8878d8ca9a84bca3dd70f3978
wandb offline

CUDA_VISIBLE_DEVICES="$DEVICES" python attn20knn_gat.py 
CUDA_VISIBLE_DEVICES="$DEVICES" python attn20knn_mpnn.py
CUDA_VISIBLE_DEVICES="$DEVICES" python attn50knn_gat.py 
CUDA_VISIBLE_DEVICES="$DEVICES" python attn50knn_mpnn.py 

CUDA_VISIBLE_DEVICES="$DEVICES" python attn20knn_critic.py 

CUDA_VISIBLE_DEVICES="$DEVICES" python attn20knn_gcn.py 

CUDA_VISIBLE_DEVICES="$DEVICES" python  attn20knn_gru.py 

CUDA_VISIBLE_DEVICES="$DEVICES" python attn20knn_transformer.py

CUDA_VISIBLE_DEVICES="$DEVICES" python   pomo20knn_gat.py 

CUDA_VISIBLE_DEVICES="$DEVICES" python   ppo20knn_gat.py 

#CUDA_VISIBLE_DEVICES="$DEVICES" python   symnco20knn_gat.py 

CUDA_VISIBLE_DEVICES="$DEVICES" python attn50knn_critic.py 

CUDA_VISIBLE_DEVICES="$DEVICES" python attn50knn_gcn.py 

CUDA_VISIBLE_DEVICES="$DEVICES" python  attn50knn_gru.py 

CUDA_VISIBLE_DEVICES="$DEVICES" python attn50knn_transformer.py

CUDA_VISIBLE_DEVICES="$DEVICES" python   pomo50knn_gat.py 

CUDA_VISIBLE_DEVICES="$DEVICES" python   ppo50knn_gat.py 

#CUDA_VISIBLE_DEVICES="$DEVICES" python   symnco50knn_gat.py 











